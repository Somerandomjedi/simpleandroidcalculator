package com.somerandomjedi.ben.testcalc;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Devide By Zero in Main Activity";
    private static final String TAG1 = "Equals Error in Main Activity";
    private static final String TAG2 = "No number specifyed in Main Activity";

    Button equals;

    EditText numField;

    int Firstnumber;
    int SecondNumber;

    boolean plus = false;
    boolean minus = false;
    boolean multi = false;
    boolean devide = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        equals = (Button) findViewById(R.id.EqualsButton);
        numField = (EditText) findViewById(R.id.editText);
    }

    // This is our 'onClick' event handler for each button
    public void handleButtonPress(View v)
    {
        // If we were triggered by the launch image view button...
        if (v.getId() == R.id.PlusButton){

            numHelp();

            plus = true;
            minus = false;
            multi = false;
            devide = false;
        }

        if (v.getId() == R.id.MinusButton){

            numHelp();

            plus = false;
            minus = true;
            multi = false;
            devide = false;
        }

        if (v.getId() == R.id.MultiplacationButton){

            numHelp();

            plus = false;
            minus = false;
            multi = true;
            devide = false;
        }

        if (v.getId() == R.id.DevisionButton){

            numHelp();

            plus = false;
            minus = false;
            multi = false;
            devide = true;
        }

        if (v.getId() == R.id.EqualsButton){
            try {
                SecondNumber = Integer.parseInt(numField.getText().toString());
                doMath();
            }catch(Exception e){
                Log.e(TAG1, " " + e.toString());
                Toast toast = Toast.makeText (getApplicationContext(), "Equals Error, Application Rest", Toast.LENGTH_LONG);
                toast.show();
                clearApp();
            }
        }
        if(v.getId() == R.id.ClearButton){
            clearApp();
        }
    } // End of handleButtonPress method

    void clearApp(){
        Firstnumber = 0;
        SecondNumber = 0;
        numField.setText("");
        plus = false;
        minus = false;
        multi = false;
        devide = false;
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    void numHelp(){
        try {
            Firstnumber = Integer.parseInt(numField.getText().toString());
        }catch(Exception e){
            Log.e(TAG2, " " + e.toString());
        }
        numField.setText("");
    }

    void doMath(){
        if(plus == true){
            long totalNumber  = Firstnumber + SecondNumber;

            numField.setText(Long.toString(totalNumber));
            numField.setSelection(numField.getText().length());

        }
        if(minus == true){
            long totalNumber  = Firstnumber - SecondNumber;

            numField.setText(Long.toString(totalNumber));
            numField.setSelection(numField.getText().length());

        }
        if(multi == true){
            long totalNumber  = Firstnumber * SecondNumber;

            numField.setText(Long.toString(totalNumber));
            numField.setSelection(numField.getText().length());

        }
        if(devide == true){
            try {
                long totalNumber = Firstnumber / SecondNumber;
                numField.setText(Long.toString(totalNumber));
                numField.setSelection(numField.getText().length());
            }catch(Exception e) {
                Log.e(TAG, " " + e.toString());
                Toast toast = Toast.makeText (getApplicationContext(), "Devide by zero", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
}
